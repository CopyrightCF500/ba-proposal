\contentsline {section}{\numberline {1}Motivation}{3}{}%
\contentsline {subsection}{\numberline {1.1}State of the art}{3}{}%
\contentsline {subsection}{\numberline {1.2}Problem Statement}{4}{}%
\contentsline {subsection}{\numberline {1.3}Research question}{4}{}%
\contentsline {subsection}{\numberline {1.4}Evaluation}{4}{}%
\contentsline {section}{\numberline {2}Outline}{5}{}%
\contentsline {section}{\numberline {3}Time plan}{6}{}%
\contentsline {section}{\numberline {4}Literature review}{6}{}%
\contentsline {section}{\numberline {5}Optional additions}{6}{}%
\contentsline {subsection}{\numberline {5.1}Trajectory output}{6}{}%
\contentsline {subsection}{\numberline {5.2}Lane detection with additional deep learning}{6}{}%
\contentsline {subsection}{\numberline {5.3}Lane detection with LIDAR}{6}{}%
